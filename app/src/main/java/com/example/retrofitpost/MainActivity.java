package com.example.retrofitpost;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.example.retrofitpost.model.UserDataModel;
import com.example.retrofitpost.model.UserWrapper;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    private ArrayList<UserDataModel> users = new ArrayList<>();
    private ArrayList<UserWrapper> users1 = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);
        users = new ArrayList<>();

        postData();
    }

    private void postData() {
        Method method = RetrofitInstance.getRetrofitInstance().create(Method.class);
        Call<UserWrapper> call = method.getUserInformation("11409");
//        Call<User> call = method.getUserInformationTest("11409","21.0","88.0","1");
        call.enqueue(new Callback<UserWrapper>() {
            @Override
            public void onResponse(@NonNull Call<UserWrapper> call, @NonNull Response <UserWrapper>response) {
                Log.e(TAG, "onResponse: "+response.body() );

            UserWrapper userWrapper = response.body();
            users=userWrapper.getResponse();
                assert userWrapper != null;
                setDataInRecyclerView();
                Log.e("data_response","onResponse: id: "+userWrapper.getResponse().size());
//                Log.e("data_response","onResponse: id: "+userWrapper.getResponse().get(0).getComplain_id());
//                Log.e("data_response","onResponse: name: "+userWrapper.getResponse().get(0).getName());
//                Log.e("data_response","onResponse: mobile: "+userWrapper.getResponse().get(0).getMobile_no());
//                Log.e("data_response","onResponse: reason: "+userWrapper.getResponse().get(0).getComplain_reason());
//                Log.e("data_response","onResponse: remarks: "+userWrapper.getResponse().get(0).getComplain_remarks());
//                Log.e("data_response","onResponse: url: "+userWrapper.getResponse().get(0).getUrl());
//                Log.e("data_response","onResponse: photo: "+userWrapper.getResponse().get(0).getComplain_photo());
//                Log.e("data_response","onResponse: status: "+userWrapper.getResponse().get(0).getComplain_status());
//                Log.e("data_response","onResponse: date: "+userWrapper.getResponse().get(0).getCreatedAt());

            }

            @Override
            public void onFailure(@NonNull Call<UserWrapper> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t.getMessage());

            }
        });
    }

    private void setDataInRecyclerView() {
        Log.e(TAG, "setDataInRecyclerView: user data"+ users.size() );
//        Log.e(TAG, "setDataInRecyclerView: user data"+  users1.get(0).getResponse().size());
        linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        RecyclerAdapter adapter = new RecyclerAdapter(MainActivity.this, users);
        recyclerView.setAdapter(adapter);
    }
}