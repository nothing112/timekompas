package com.example.retrofitpost;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

public class EditActivity extends AppCompatActivity {
    EditText name,mobile,reason,remark,date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        name = findViewById(R.id.name);
        mobile = findViewById(R.id.mobile);
        reason = findViewById(R.id.reason);
        remark = findViewById(R.id.remark);
        date = findViewById(R.id.date);

       String Name = getIntent().getStringExtra("name");
       name.setText(Name);

       String Mobile = getIntent().getStringExtra("mobile");
       mobile.setText(Mobile);

       String Reason = getIntent().getStringExtra("reason");
       mobile.setText(Reason);

       String Remark = getIntent().getStringExtra("remark");
       mobile.setText(Remark);

       String Date = getIntent().getStringExtra("date");
       mobile.setText(Date);

    }
}