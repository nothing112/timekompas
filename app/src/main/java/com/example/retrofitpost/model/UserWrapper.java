package com.example.retrofitpost.model;

import java.util.ArrayList;

public class UserWrapper {
   String status;
   ArrayList<UserDataModel> response;

   public UserWrapper(String status, ArrayList<UserDataModel> response) {
      this.status = status;
      this.response = response;
   }

   public String getStatus() {
      return status;
   }

   public void setStatus(String status) {
      this.status = status;
   }

   public ArrayList<UserDataModel> getResponse() {
      return response;
   }

   public void setResponse(ArrayList<UserDataModel> response) {
      this.response = response;
   }
}
