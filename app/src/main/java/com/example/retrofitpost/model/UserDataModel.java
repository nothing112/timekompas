package com.example.retrofitpost.model;

public class UserDataModel {

    private String complainIid;
    private String Name;
    private String mobileNo;
    private String complainReason;
    private String complainRemarks;
    private String Url;
    private String complainPhoto;
    private String complainStatus;
    private String CreatedAt;

    public String getComplainIid() {
        return complainIid;
    }

    public void setComplainIid(String complainIid) {
        this.complainIid = complainIid;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getComplainReason() {
        return complainReason;
    }

    public void setComplainReason(String complainReason) {
        this.complainReason = complainReason;
    }

    public String getComplainRemarks() {
        return complainRemarks;
    }

    public void setComplainRemarks(String complainRemarks) {
        this.complainRemarks = complainRemarks;
    }

    public String getComplainPhoto() {
        return complainPhoto;
    }

    public void setComplainPhoto(String complainPhoto) {
        this.complainPhoto = complainPhoto;
    }

    public String getComplainStatus() {
        return complainStatus;
    }

    public void setComplainStatus(String complainStatus) {
        this.complainStatus = complainStatus;
    }

    private String complain_id;
    private String name;
    private String mobile_no;
    private String complain_reason;
    private String complain_remarks;
    private String url;
    private String complain_photo;
    private String complain_status;
    private String createdAt;

    public UserDataModel(String complain_id, String name, String mobile_no, String complain_reason, String complain_remarks, String url, String complain_photo, String complain_status, String createdAt) {
        this.complain_id = complain_id;
        this.name = name;
        this.mobile_no = mobile_no;
        this.complain_reason = complain_reason;
        this.complain_remarks = complain_remarks;
        this.url = url;
        this.complain_photo = complain_photo;
        this.complain_status = complain_status;
        this.createdAt = createdAt;
    }

    public String getComplain_id() {
        return complain_id;
    }

    public void setComplain_id(String complain_id) {
        this.complain_id = complain_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getComplain_reason() {
        return complain_reason;
    }

    public void setComplain_reason(String complain_reason) {
        this.complain_reason = complain_reason;
    }

    public String getComplain_remarks() {
        return complain_remarks;
    }

    public void setComplain_remarks(String complain_remarks) {
        this.complain_remarks = complain_remarks;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getComplain_photo() {
        return complain_photo;
    }

    public void setComplain_photo(String complain_photo) {
        this.complain_photo = complain_photo;
    }

    public String getComplain_status() {
        return complain_status;
    }

    public void setComplain_status(String complain_status) {
        this.complain_status = complain_status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
