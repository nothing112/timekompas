package com.example.retrofitpost;

import com.example.retrofitpost.model.UserWrapper;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface Method {

    @FormUrlEncoded
    @POST("api/user/getcomplainrequest?emp_id=11409")
    Call<UserWrapper> getUserInformation(@Field("emp_id") String emp_id );

    @FormUrlEncoded
    @POST("api/shyam/getattendanceintegratedata")
    Call<UserWrapper> getUserInformationTest(
            @Field("empid") String emp_id,
            @Field("lat") String lat,
            @Field("lon") String lon,
            @Field("attendmode") String attendmode
            );
}
