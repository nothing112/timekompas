package com.example.retrofitpost;


import android.content.Intent;
import android.nfc.Tag;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.retrofitpost.model.UserDataModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder>{
    private ArrayList<UserDataModel> user;
    private MainActivity mainActivity;
    private ImageView editPen;

    public RecyclerAdapter(MainActivity mainActivity, ArrayList<UserDataModel> user) {
        this.user = user;
        this.mainActivity = mainActivity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.recyclerview, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        UserDataModel data = user.get(position);
        holder.name.setText(data.getName());
        Log.e("TAG1", data.getCreatedAt()+"onBindViewHolder: "+data.getMobileNo() );
        holder.mobile.setText(data.getMobile_no());
        holder.reason.setText(data.getComplain_reason());
        holder.status.setText(data.getComplain_remarks());
        holder.date.setText(data.getCreatedAt());
//        holder.image.setOnClickListener();

        Picasso.with(mainActivity.getApplicationContext())
                .load(data.getUrl())
                .placeholder(R.drawable.ic_launcher_background)
                .fit()
                .into(holder.image);

        editPen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(editPen.getContext(), "sucess", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(mainActivity,EditActivity.class);
                intent.putExtra("name",data.getName());
                intent.putExtra("mobile",data.getMobile_no());
                intent.putExtra("reason",data.getComplain_reason());
                intent.putExtra("remark",data.getComplain_remarks());
                intent.putExtra("date",data.getCreatedAt());
                mainActivity.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return user.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name,mobile,reason,status,date;
        ImageView image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            mobile = itemView.findViewById(R.id.mobile);
            reason = itemView.findViewById(R.id.reason);
            status = itemView.findViewById(R.id.status);
            date = itemView.findViewById(R.id.date);
            image = itemView.findViewById(R.id.image);
            editPen = itemView.findViewById(R.id.editPen);

//            editPen.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Toast.makeText(editPen.getContext(), "success", Toast.LENGTH_SHORT).show();
//
//                    Intent intent = new Intent(mainActivity,EditActivity.class);
//                    intent.putExtra("name",data.getName());
//                    intent.putExtra("mobile",data.getMobile_no());
//                    intent.putExtra("reason",data.getComplain_reason());
//                    intent.putExtra("remark",data.getComplain_remarks());
//                    intent.putExtra("date",data.getCreatedAt());
//                    mainActivity.startActivity(intent);
//                }
//            });
            

        }
    }
}
